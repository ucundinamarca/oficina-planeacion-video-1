/**
 * @author Edilson Laverde Molina
 * @email edilsonlaverde_182@hotmail.com
 * @create date 2020-12-12 18:45:48
 * @modify date 2020-12-18 09:48:07
 * @desc Videos REA3
 */
var videos = [{
    "id": "VeHDu8ANypo",
    "titulo": "Conocimiento de Soborno y Corrupción Parte 1 Entendiendo Soborno y Corrupción Video 1",
    "descripcion": "",
    "url": "VeHDu8ANypo",
    "preguntas": [
        {
            "second": "27",
            "question": "¿Para ti qué es corrupción?"
        }, {
            "second": "86",
            "question": "¿Sabes cuales formas puede tomar el soborno?"
        }, {
            "second": "120",
            "question": "¿Conoces el Código Autonómico “Universidad de Cundinamarca Generación Siglo XXI”",
            "answers": ["Si","No"]
        }
    ]
}
];    

